<?php
class Database {
    private $isConn;
    private $db_name = 'slug';
    private $db_user = 'root';
    private $db_pass = '';
    private $db_host = 'localhost';
    private $char = 'utf8';
    private $dbh;

    public function __construct() {
        if (!isset($this->dbh)) {
            //set data source name
            $dsn = 'mysql:host=' . $this->db_host . ';dbname=' . $this->db_name .';charset='.$this->char;
            //set Custom PDO options.
            $options = array(PDO::ATTR_PERSISTENT => true);
            // Create a new PDO instanace
            try {
                $link = new PDO($dsn, $this->db_user, $this->db_pass, $options);
                $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                //set the default set mode here
                //$link->setFetchMode(PDO::FETCH_ASSOC);
                $this->dbh = $link;
                $this->isConn = TRUE;
            } catch (PDOException $exc) {
                echo "Failed to connect with database" . $exc->getMessage();
            }
        }
    }



    //Returns the number of rows affected by the last SQL statement
    public function rowCounts($query, $params = []) {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return $sth->rowCount();
        } catch (PDOException $exc) {
            echo 'Error' . $exc->getMessage();
        }
    }
    

    //get single row
    public function getRow($query, $params = []) {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return $sth->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $exc) {
            echo 'Error' . $exc->getMessage();
        }
    }

    // get multiple rows
    public function getRows($query, $params = []) {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $exc) {
            echo 'Error' . $exc->getMessage();
        }
    }

    // insert row
    public function insertRow($query, $params = []) {

        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return TRUE;
        } catch (PDOException $exc) {
            echo 'Error' . $exc->getMessage();
        }
    }
    // update row
    public function updateRow($query, $params = []){
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return $sth->rowCount();
        } catch (PDOException $exc) {
            echo 'Error' . $exc->getMessage();
        }
    }
    // delete row
    public function deleteRow($query, $params = []){
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return TRUE;
        } catch (PDOException $exc) {
            echo 'Error' . $exc->getMessage();
        }
    }
}?>
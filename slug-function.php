<?php

/*
 * Create Web friendly slug
 *
 * A custom function in PHP which lets you create a web friendly URL slug from a string.
 * NOTE: transliteration may causes a loss of information
 *
 * @param string $str
 * @param string $delimiter
 * @param int $limit limit to truncate slug to maximum characters
 * @param bool $lowercase to convert slug to lowercase
 * @return string
 * */

function slug($string, $delimiter = '-', $lowercase = true, $limit = 255) {


    /*
     * Replace non-alphanumeric characters with our delimiter
     */
    //$string = preg_replace('/[^\p{L}\p{Nd}]+/u', $delimiter, $string);
    $string = preg_replace('/[\s_]+/u', '-', trim($string));

    /*
     *  Remove duplicate delimiters
     */
    //$string = preg_replace('/(' . preg_quote($delimiter, '/') . '){2,}/', '$1', $string);

    /*
     * Trim slug to max characters
     */
    $string = mb_substr($string, 0, ($limit ? $limit : mb_strlen($string, 'UTF-8')), 'UTF-8');

    /*
     * Remove delimiter from ends
     */
    $string = trim($string, $delimiter);

    return $lowercase ? mb_strtolower($string, 'UTF-8') : $string;
}

//works with bangla text
function make_slug($string) {
    return preg_replace('/\s+/u', '-', trim($string));
}
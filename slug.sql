-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 22, 2018 at 07:45 PM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `slug`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slug`
--

DROP TABLE IF EXISTS `tbl_slug`;
CREATE TABLE IF NOT EXISTS `tbl_slug` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug_title` varchar(255) NOT NULL,
  `slug_url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_url` (`slug_url`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_slug`
--

INSERT INTO `tbl_slug` (`id`, `slug_title`, `slug_url`) VALUES
(1, 'আমার নাম সাব্বির হোসেন', 'আমার-নাম-সাব্বির-হোসেন'),
(2, 'আমার নাম সাব্বির হোসেন', 'আমার-নাম-সাব্বির-হোসেন-1'),
(3, 'আমার নাম সাব্বির হোসেন', 'আমার-নাম-সাব্বির-হোসেন-2'),
(4, 'বাংলাদেশ-ব্যাংকের-রিজার্ভের-অর্থ-চুরির-ঘটনায়-ফিলিপাইনের', 'বাংলাদেশ-ব্যাংকের-রিজার্ভের-অর্থ-চুরির-ঘটনায়-ফিলিপাইনের'),
(5, 'আমার নাম সাব্বির', 'আমার-নাম-সাব্বির'),
(6, 'আমার নাম সাব্বির', 'আমার-নাম-সাব্বির-1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

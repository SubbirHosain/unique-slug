<?php
include "db/database.php";
include "slug-function.php";
$dbh = new Database();
    $slug ='';
    $msg = '';
    if (isset($_POST['create'])){

        $slug_title = $_POST['slug_title'];

        $slug = slug($slug_title,'-',true,60);

        //checking slug url before inserting into db
        $sql = "SELECT slug_url FROM tbl_slug WHERE slug_url LIKE '$slug%'";
        //$slug_data = array($slug);

        $total_row = $dbh->rowCounts($sql);

        if ($total_row>0){
            $result = $dbh->getRows($sql);

            //store them in an array
            foreach ($result as $row) {
                $data[]= $row['slug_url'];
            }

            //increase the slug url value if exists
            if (in_array($slug,$data)){
                $count = 0;
                while (in_array(($slug .'-' . ++$count ),$data) );
                $slug = $slug . '-' . $count;
            }

        }

        //insert unique slug after checking
        $sql_slug_insert = "INSERT INTO tbl_slug (slug_title, slug_url) VALUES (?,?)";
        $insert_data = array($slug_title,$slug);
        if($dbh->insertRow($sql_slug_insert,$insert_data)){
            $msg = "inserted";
        }
    }

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create php unique url slug</title>
</head>
<body>
    <center>
        <h2>creating slug from string</h2>

        <form action="" method="post">
            <table>
                <tr>
                    <td><input type="text" name="slug_title" id=""></td>
                </tr>
                <tr>
                    <td><input type="submit" name="create" value="Create"></td>
                </tr>
                <tr>
                    <td>Output Slug:<?php echo $slug?></td>
                </tr>
                <tr>
                    <td><?php echo $msg; ?></td>
                </tr>
            </table>
        </form>
        <br>
        <br>
        <?php echo make_slug(" বাংলাদেশ   ব্যাংকের    রিজার্ভের  অর্থ  চুরির   ঘটনায়   ফিলিপাইনের  "); ?>
        <p>
            <a href="user.php">user/name</a>
        </p>
        <p>
            <a href="post.php">post/title</a>
        </p>
    </center>
</body>
</html>